-module(classic_prims).

-export([lior/2, land/2, lnot/1]).

lior(0, 0) -> 0;
lior(0, 1) -> 1;
lior(1, 0) -> 1;
lior(1, 1) -> 1.

land(0, 0) -> 0;
land(0, 1) -> 0;
land(1, 0) -> 0;
land(1, 1) -> 1.

lnot(0) -> 1;
lnot(1) -> 0.
