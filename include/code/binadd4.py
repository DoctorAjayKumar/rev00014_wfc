#!/usr/bin/env python3

'''
Note: x // y is integer division in python 3

4-digit binary adder truth table
'''

def place(num, place):
    '''
    Find the 'place'-ths place digit for number 'num' in binary

    So the `1`-s place digit of 7 is place(7, 1)

    So the `4`-s place digit of 7 is place(7, 4)

    Returns

    (num // place) % 2

    If you give it some weird shit input, like place not being a power of 2,
    might do weird shit, idk.
    '''
    return (num // place) % 2


def table():
    print('A8,A4,A2,A1,B8,B4,B2,B1,|,V16,V8,V4,V2,V1')
    #print('-----------------------------------------------------')
    for i in range(256):
        A8 = place(i, 128)
        A4 = place(i,  64)
        A2 = place(i,  32)
        A1 = place(i,  16)
        B8 = place(i,   8)
        B4 = place(i,   4)
        B2 = place(i,   2)
        B1 = place(i,   1)

        A = i // 16
        B = i % 16

        V = A + B

        V16 = place(V, 16)
        V8  = place(V,  8)
        V4  = place(V,  4)
        V2  = place(V,  2)
        V1  = place(V,  1)

        print(
            #'%2d %3d %3d %3d %3d %3d %3d %3d  | %3d %3d %3d %3d %3d' %
            '%d,%d,%d,%d,%d,%d,%d,%d,|,%d,%d,%d,%d,%d' %
                ( A8
                , A4
                , A2
                , A1
                , B8
                , B4
                , B2
                , B1
                , V16
                , V8
                , V4
                , V2
                , V1
                )
            )

if __name__ == '__main__':
    table()
